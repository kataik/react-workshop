import { createAction } from 'redux-actions'

class Action {
  constructor(name) {
    this.name = name;
    this.reduxAction = createAction(this.name);
  }

  generate(payload) {
    return this.reduxAction(payload);
  }
}

export default Action;
