import Action from './Action'

class ApiAction {
  static postfixes = {
    trigger: '',
    cancel: '_CANCEL',
    ok: '_FULFILLED',
    reject: '_REJECTED'
  }

  constructor(name) {
    this.name = name;

    Object.keys(ApiAction.postfixes).forEach(key => this[key] = new Action(this.name + ApiAction.postfixes[key]), this);
  }
}

export default ApiAction;
