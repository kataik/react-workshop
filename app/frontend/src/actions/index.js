import Action from './Action'
import ApiAction from './ApiAction'

export const healthCheckAction = new ApiAction('HEALTHCHECK_FETCH');
export const addReservationAction = new Action('ADD_RESERVATION');
export const removeReservationAction = new Action('REMOVE_RESERVATION');
export const clearReservationAction = new Action('CLEAR_RESERVATION');
export const submitReservationAction = new ApiAction('SUBMIT_RESERVATION');
export const refreshBookingStateAction = new ApiAction('REFRESH_BOOKING_STATE');
