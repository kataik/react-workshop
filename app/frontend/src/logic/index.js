import {
  healthCheckAction,
  submitReservationAction,
  refreshBookingStateAction
} from '../actions'

import RestRequestLogic from './RestRequestLogic'

const isDevelopment = process.env.NODE_ENV === 'development';
const backendPort = isDevelopment ? process.env.REACT_APP_BACKEND_PORT ? process.env.REACT_APP_BACKEND_PORT : 3001 : 3000;
const devConfig = { fetchConfig: { mode: 'cors' } };

const healthCheckRequestLogic = new RestRequestLogic(isDevelopment ? `http://localhost:${backendPort}/api/healthcheck` : '/api/healthcheck');
const bookingLogic = new RestRequestLogic(isDevelopment ? `http://localhost:${backendPort}/api/booking` : '/api/booking');

export default [
  healthCheckRequestLogic.get(healthCheckAction, isDevelopment ? devConfig : { }), 
  bookingLogic.get(refreshBookingStateAction, isDevelopment ? devConfig : {}),
  bookingLogic.post(submitReservationAction, isDevelopment ? devConfig : {}) ];
