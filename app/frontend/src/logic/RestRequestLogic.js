import merge from 'deepmerge'
import { createLogic } from 'redux-logic'

class RestRequestLogic {
  // https://github.com/jeffbski/redux-logic/blob/master/docs/api.md
  static defaultLogicConfig = {
    latest: true,

    processOptions: {
      dispatchReturn: true
    }
  }

  // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  static defaultFetchConfig = {
    mode: "same-origin",
    cache: "default",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    },
    redirect: "follow",
    referrer: "client"
  }

  constructor(resourcePath) {
    this.path = resourcePath;
  }

  get(action, config = {}) {
    return this._buildRequestLogic(
      action,
      { method: 'GET' },
      {},
      config);
  }

  post(action, config = {}) {
    return this._buildRequestLogic(
      action,
      { method: 'POST' },
      {},
      config);
  }

  put(action, config = {}) {
    return this._buildRequestLogic(
      action,
      { method: 'PUT' },
      {},
      config);
  }

  del(action, config = {}) {
    return this._buildRequestLogic(
      action,
      { method: 'DELETE' },
      {},
      config);
  }

  _buildSender(path, fetchConfig) {
    return async (id, data) => {
      const url = id ? path + `/${id}` : path;
      fetchConfig = data ? this._merge(fetchConfig, { body: JSON.stringify(data) }) : fetchConfig;

      const rsp = await fetch(url, fetchConfig);

      if (!rsp.ok)
        throw Error(rsp.statusText);

      return await rsp.json();
    };
  }

  _buildRequestLogic(action, fetchPreset = {}, configPreset = {}, config = {}) {
    const sender = this._buildSender(
      this.path,
      merge.all([
        RestRequestLogic.defaultFetchConfig,
        fetchPreset,
        config.fetchConfig || {}]));

    return createLogic(
      merge.all([
        RestRequestLogic.defaultLogicConfig,
        this._actionConfig(action),
        {
          async process({ action }) {
            return await action.payload ? sender(action.payload.id, action.payload.data) : sender();
          }
        },
        configPreset,
        config.logicConfig || {}]));
  }

  _actionConfig(action) {
    return {
      type: action.trigger.name,
      cancelType: action.cancel.name,

      processOptions: {
        successType: (data) => action.ok.generate(data),
        failType: (err) => action.reject.generate(err)
      }
    }
  }
}

export default RestRequestLogic;
