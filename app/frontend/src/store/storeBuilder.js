import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger'
import { createLogicMiddleware } from 'redux-logic';
import rootReducer from '../reducers';
import logic from '../logic';

const deps = {
  // redux-logic middleware dependencies, see: https://github.com/jeffbski/redux-logic#usage
};

const logicMiddleware = createLogicMiddleware(logic, deps);
const logger = createLogger();

const middleware = process.env.NODE_ENV === 'development' ? applyMiddleware(logicMiddleware, logger) : applyMiddleware(logicMiddleware);

const configureStore = () => createStore(rootReducer, middleware);

export default configureStore;
