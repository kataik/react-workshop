import React from 'react'
import PropTypes from 'prop-types'
import './TriStateButton.scss'

const TriStateButton = ({state, children, id, onClick}) => {
    const stateToStyle = (state) => {
        switch (state) {
            case undefined:
                return "empty";
            case true:
                return "enabled";
            case false:
                return "disabled";
            default:
                throw Error(`state ${state} not supported`);
        }
    }

    const clickHandler = (e) => {
        e.preventDefault();
        onClick(state);
    }

    return (
        <button className={stateToStyle(state)} onClick={clickHandler.bind(this)} disabled={state === undefined}>{children}</button>
    );
}

TriStateButton.prototype.propTypes = {
    state: PropTypes.oneOf([undefined, true, false]).isRequired,
    onClick: PropTypes.func.isRequired
}

export default TriStateButton;
