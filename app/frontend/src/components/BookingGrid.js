import {
    submitReservationAction,
    refreshBookingStateAction
} from '../actions'

import { connect } from 'react-redux'
import ButtonGrid from './ButtonGrid'

const BookingGrid = connect(
    (state, ownProps) => ({ layout: state.booking.room ? state.booking.room.seats : null, ...ownProps }),
    (dispatch) => ({
        refresh: () => dispatch(refreshBookingStateAction.trigger.generate()),
        submit: (reservations) => dispatch(submitReservationAction.trigger.generate(reservations))
    })
)(ButtonGrid)

export default BookingGrid;