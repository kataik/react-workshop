import React from 'react'
import './App.scss'
import { HealthCheck, Header, BookingGrid } from './'

const App = () => (
  <div className="App">
    <Header>Booking</Header>
    {process.env.NODE_ENV === 'development' ? <HealthCheck /> : null}
    <BookingGrid refreshInterval={5000} />
  </div>
);

export default App;
