import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { HealthCheckShape } from './shapes'
import './HealthCheck.scss'

class HealthCheckWidgetView extends Component {
  static propTypes = {
    healthCheck: HealthCheckShape.isRequired,
    fetchHealthCheck: PropTypes.func.isRequired
  }

  componentDidMount() {
    this.props.fetchHealthCheck();
  }

  renderReport(healthCheck) {
    if (healthCheck.fetching)
      return ( <div className="segment">Loading...</div> );
    else if (healthCheck.error)
      return ( <div className="segment"><span>Error: </span>{healthCheck.error.message}</div> );
    else if (healthCheck.value)
      return (
        <React.Fragment>
          <div className="segment">Status: {healthCheck.value.status}{healthCheck.value.status !== 'pass' ? "- " + healthCheck.value.output : ""}</div>
          <div className="segment">Version: {healthCheck.value.version}</div>
          <div className="segment">Details: {healthCheck.value.details ? JSON.stringify(healthCheck.value.details) : "N/A"}</div>
          <div className="segment">Notes: {healthCheck.value.notes && healthCheck.value.notes.length > 0 ? healthCheck.value.notes.join(', ') : "N/A"}</div>
        </React.Fragment>
      );
    else
      return (
        <div className="segment">N/A</div>
      );
  }

  render() {
    return (
      <article className="HealthCheckWidget-box">
        <button onClick={this.props.fetchHealthCheck}>Reload</button>
        <div className="report">
          {this.renderReport(this.props.healthCheck)}
        </div>
      </article>
    );
  }
}

export default HealthCheckWidgetView;
