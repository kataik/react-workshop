import PropTypes from 'prop-types'

export const HealthCheckDataShape = PropTypes.shape({
  status: PropTypes.oneOf(['pass', 'fail', 'warn']).isRequired,
  version: PropTypes.string,
  releaseID: PropTypes.string,
  notes: PropTypes.array,
  output: PropTypes.string,
  details: PropTypes.object,
  links: PropTypes.array,
  serviceID: PropTypes.string,
  description: PropTypes.string
})

export const HealthCheckShape = PropTypes.shape({
  value: HealthCheckDataShape,
  fetching: PropTypes.bool.isRequired,
  error: PropTypes.object
})
