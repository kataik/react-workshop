import {
    addReservationAction,
    removeReservationAction
} from '../actions'

import {connect} from 'react-redux'
import TriStateButton from './TriStateButton'

const BookingButton = connect(
    (state, ownProps) => ({ 
        state: state.booking.room.seats[ownProps.row][ownProps.seat].state !== "empty"
            ? undefined
            : state.booking.reservation.includes(ownProps.id),
        ...ownProps
    }),
    (dispatch, ownProps) => ({
        onClick: (state) => dispatch(state
            ? removeReservationAction.generate(ownProps.id)
            : addReservationAction.generate(ownProps.id))
    })
)(TriStateButton)

export default BookingButton;