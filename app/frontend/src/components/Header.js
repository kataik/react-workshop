import React from 'react';
import './Header.scss';

const Header = (props) => (
    <header className="Header-box">
        {props.children}
    </header>
);

export default Header;