import {
  healthCheckAction
} from '../actions'

import { connect } from 'react-redux';
import HealthCheck from './HealthCheck'

const ApiHealthCheck = connect(
  (state) => ({ healthCheck: state.healthCheck }),
  (dispatch) => ({
    fetchHealthCheck: () => dispatch(healthCheckAction.trigger.generate())
  }))(HealthCheck);

export default ApiHealthCheck;
