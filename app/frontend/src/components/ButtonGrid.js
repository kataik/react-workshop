import React, { Component } from 'react'
import { BookingButton } from './'
import './ButtonGrid.scss'
import LoadingPlaceholder from './resources/loading.png'

class ButtonGrid extends Component {
    componentDidMount() {
        this.setState({ intervalId: setInterval(this.refresh.bind(this), this.props.refreshInterval) });
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId);
    }

    refresh() {
        this.props.refresh();
    }

    render() {
        if (!this.props.layout) {
            return (
                <div className="ButtonGrid-box">
                    <img src={LoadingPlaceholder} alt="placeholder" className="ButtonGrid-loading" />
                </div>
            );
        }

        return (
            <div className="ButtonGrid-box">
                <div className="ButtonGrid-layout">
                    {Object.keys(this.props.layout).map(row => (
                        <div className="ButtonGrid-row" key={row}>
                            <label className="ButtonGrid-label">{row}</label>
                            {Object.keys(this.props.layout[row]).map(seat => (
                                <div className="ButtonGrid-seat" key={`${row}_${seat}`}>
                                    <BookingButton row={row} seat={seat} id={`${row}_${seat}`}>{this.props.layout[row][seat].type}</BookingButton>
                                </div>
                            ))}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default ButtonGrid;