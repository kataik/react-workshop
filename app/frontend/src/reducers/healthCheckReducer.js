import {
  healthCheckAction
} from '../actions';

export default (state = { value: null, fetching: false, error: null }, action) => {
  switch (action.type) {
    case healthCheckAction.trigger.name:
      return { ...state, fetching: true };
    case healthCheckAction.cancel.name:
      return { ...state, fetching: false };
    case healthCheckAction.ok.name:
      return { value: action.payload, fetching: false, error: null };
    case healthCheckAction.reject.name:
      return { ...state, fetching: false, error: action.payload };
    default:
      return state;
  }
}
