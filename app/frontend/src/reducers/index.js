import { combineReducers } from 'redux';
import healthCheck from './healthCheckReducer';
import booking from './bookingReducer';

export default combineReducers({ healthCheck, booking });
