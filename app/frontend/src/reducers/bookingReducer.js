import {
  addReservationAction,
  removeReservationAction,
  clearReservationAction,
  submitReservationAction,
  refreshBookingStateAction
} from '../actions'

export default (state = { room: null, reservation: [], fetching: false, submitting: false, error: null }, action) => {
  switch (action.type) {
    case addReservationAction.name:
      return { ...state, reservation: [ ...state.reservation, action.payload ] };
    case removeReservationAction.name:
      return { ...state, reservation: state.reservation.filter(item => item !== action.payload) };
    case clearReservationAction.name:
      return { ...state, reservation: [] };
    case submitReservationAction.trigger.name:
      return { ...state, submitting: true };
    case submitReservationAction.cancel.name:
      return { ...state, submitting: false };
    case submitReservationAction.reject.name:
      return { ...state, submitting: false, error: action.payload };
    case submitReservationAction.ok.name:
      return { ...state, submitting: false, room: action.payload, reservation: [], error: null };
    case refreshBookingStateAction.trigger.name:
      return { ...state, fetching: true };
    case refreshBookingStateAction.cancel.name:
      return { ...state, fetching: false };
    case refreshBookingStateAction.reject.name:
      return { ...state, fetching: false, error: action.payload };
    case refreshBookingStateAction.ok.name:
      return {
        ...state, 
        fetching: false, 
        room: action.payload,
        reservation: state.reservation.filter(item => {
          const split = item.split("_");
          return action.payload.seats[split[0]][split[1]].state === "empty";
        }), 
        error: null
      };
    default:
      return state;
  }
}
