import React from 'react';
import ReactDOM from 'react-dom';
import store from './store';
import { Provider } from 'react-redux';
import './index.scss';
import { App } from './components';
// uncomment this if your app supposed to work offline - learn about limitations here: http://bit.ly/CRA-PWA
// import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));

// uncomment this if your app supposed to work offline - learn about limitations here: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
