#!/bin/zsh

set -e
unsetopt BG_NICE

NODE_ENV=${1:-production}
NPM_UPDATE=${2:-skip-update}

if [ $NODE_ENV = 'development' ]; then
  BACKEND_PORT=${3:-3001}
  FRONTEND_PORT=${4:-3000}
  DEBUG='backend'
else
  BACKEND_PORT=${3:-3000}
fi
  
# PORT=$(if [ $NODE_ENV = 'development' ]; then DEBUG="backend"; echo 3001; else echo 3000; fi)

echo "Mode: $NODE_ENV, backend: $BACKEND_PORT, frontend: $FRONTEND_PORT"

if [ $NPM_UPDATE = 'update' ]; then
  npm install --prefix ./backend && npm install --prefix ./frontend
fi

PORT=$BACKEND_PORT DEBUG=$DEBUG NODE_ENV=$NODE_ENV npm start --prefix ./backend &

if [ $NODE_ENV = 'development' ]; then
  PORT=$FRONTEND_PORT NODE_ENV=$NODE_ENV REACT_APP_BACKEND_PORT=$BACKEND_PORT npm start --prefix ./frontend &
fi

wait $(jobs -p)
echo "Done."
	
