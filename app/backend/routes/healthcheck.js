const router = require('express').Router();

// https://tools.ietf.org/id/draft-inadarei-api-health-check-01.html#api-health-response

router.get('/', (req, res, next) => {
  res.set("Content-Type", "application/health+json");

  res.status(200).send({
    status: "warn", // pass, warn, fail
    version: "0.1",
    notes: [ "api is not implemented" ],
    output: "API is empty", // should be omitted in case of pass
    details: {}
  });
});

module.exports = router;
