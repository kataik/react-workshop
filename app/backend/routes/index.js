const router = require('express').Router();

router.use('/healthcheck', require('./healthcheck'));
router.use('/booking', require('./booking'));

module.exports = router;
