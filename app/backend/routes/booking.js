const router = require('express').Router();
let state = require('../data/room.json')

router.get('/', (req, res, next) => {
    res.status(200).send(state);
});

router.post('/', (req, res, next) => {
  const targets = req.body.map(element => element.split("_")).map(pos => state.seats[pos[0]][pos[1]]);

  if (!targets.every(target => target.state === "empty")) {
      res.sendStatus(403);
      return;
  }

  targets.forEach(target => target.state = "occupied");
  res.status(200).send(state);
});

module.exports = router;
